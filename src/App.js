import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import image from './assets/images/footer.png';
import image_slider from './assets/images/slider.png';
import image_menu from './assets/images/menu.png';
import image_order from './assets/images/order_form.png';

const App = () => {
  return (
    <>
      <header>
        {/* Fixed navbar */}
        <nav className='navbar navbar-expand-md fixed-top'>
          <button
            className='navbar-toggler'
            type='button'
            data-toggle='collapse'
            data-target='#navbarCollapse'
            aria-controls='navbarCollapse'
            aria-expanded='false'
            aria-label='Toggle navigation'>
            <span className='navbar-toggler-icon' />
          </button>
          <div className='collapse navbar-collapse' id='navbarCollapse'>
            <ul className='navbar-nav'>
              <li className='nav-item active'>
                <a className='nav-link' href='#trang-chu' style={{ paddingLeft: "200px", paddingRight: "120px" }}>Trang chủ</a>
              </li>
              <li className='nav-item active'>
                <a className='nav-link' href='#combo' style={{ paddingLeft: "120px", paddingRight: "120px" }}>Combo</a>
              </li>
              <li className='nav-item'>
                <a className='nav-link' href='#loai-pizza' style={{ paddingLeft: "120px", paddingRight: "120px" }}>Loại Pizza</a>
              </li>
              <li className='nav-item'>
                <a className='nav-link' href='#gui-don-hang' style={{ paddingLeft: "120px", paddingRight: "200px" }}>Gửi đơn hàng</a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      {/* Begin page content */}
      <main role='main' className='flex-shrink-0'>
        <div className='container'>
          <p className='lead'>
            <img src={image_slider} width="1300px" />
            <img src={image_menu} width="1350px" />
            <img src={image_order} width="1350px" />
          </p>
        </div>
      </main>
      <footer className='footer mt-auto py-3 text-black'>
        <div className='container' style={{ textAlign: "center", paddingTop: "50px" }}>Footer</div>
        <img src={image} width="150px" style={{ paddingLeft: "30px", marginLeft: "660px" }} />
        <div className='container' style={{ textAlign: "center", paddingBottom:"85px" }}>Powered by DEVCAMP</div>
      </footer>
    </>
  )
}

export default App